import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 9:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class Employee extends Person{
    private Employee supervisor;
    private ArrayList<Employee> employees = new ArrayList<Employee>();
    private String job = "";


    public Employee(String n, String a, int i){
        super(n, a, i);
    }

    public void setJob(String j){
        job = j;
    }

    public String getJob(){
        return job;
    }

    public void setEmp(Employee e){
        supervisor = e;
    }

    public ArrayList<Employee> getEmp(){
        return employees;
    }

    public void addEmp(Employee e){
        employees.add(e);
    }

    public void remEmp(Employee e){
        employees.remove(e);
    }

}
