/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 9:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class Person{
    private String name = "";
    private int id = 0;
    private String address;

    public Person(String n, String a, int i){
        name = n;
        address = a;
        id = i;
    }

    public String getName(){
        return name;
    }

    public String getAddress(){
        return address;
    }

    public int getId(){
        return id;
    }

    public String toString(){
        return name;
    }
}
