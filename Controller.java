import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 12/2/13
 * Time: 3:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Controller {
    private ArrayList<Airline> airlines = new ArrayList<Airline>();
    private ArrayList<Person> persons = new ArrayList<Person>();

    /**
     * Create a new airline
     * @param name the name of the new airline
     * @return true if the airline doesn't exists false if it already exists
     */
    public boolean newAirline(String name){
        for(Airline a : airlines){
            if(a.getName().equals(name)){
                return false;
            }
        }
        airlines.add(new Airline(name));
        return true;
    }

    /**
     * Create a new RegularFlight
     * @param airlineID the id of the airline
     * @param flightNumber the number of the flight
     * @param depTime the department time
     * @param dep the department location
     * @param arr the arrivel location
     * @return true if the new regularflight is created
     */
    public boolean newRF(int airlineID, int flightNumber, String depTime, String dep, String arr){
        Airline al = airlines.get(airlineID);
        for(RegularFlight rf : al.getFlights()){
            if(rf.getFlightNumber() == flightNumber){
                return false;
            }
        }
        al.addFlight(new RegularFlight(al, flightNumber, depTime, dep, arr));
        return true;
    }

    /**
     * Create a new specificflight and adds it to the list
     * @param alID The id of the airline
     * @param fn The flightnumber of the flight
     * @param date the date of the flight
     * @return true if the regularflight is enabled
     */
    public boolean newSF(int alID, int fn, String date){
        Airline al = airlines.get(alID);
        for(RegularFlight rf : al.getFlights()){
            if(rf.getFlightNumber() == fn){
                rf.addFlight(new SpecificFlight(rf, date));
                return true;
            }
        }
        return false;
    }

    /**
     * Create a person and add it to the list
     * @param name the name of the person
     * @param address the address of the person
     * @param id idea of the person
     * @return true if the person has been added
     */
    public boolean addPerson(String name, String address, int id){
        for(Person p : persons){
            if(p.getId() == id){
                return false;
            }
        }
        persons.add(new Person(name, address, id));
        return true;
    }

    /**
     * Create a new employee and add it to the list
     * @param name the name of the employee
     * @param address the address of the employee
     * @param id the id number of the employee
     * @return 0 if it failed 1 if it succeeded and 2 if the person is converted to employee
     */
    public int addEmployee(String name, String address, int id){
        for(Person p : persons){
            if(p.getId() == id){
                if(p instanceof Employee){
                    return 0;
                }else{
                    persons.remove(p);
                    persons.add(new Employee(name, address, id));
                    return 2;
                }
            }
        }
        persons.add(new Employee(name, address, id));
        return 1;
    }

    /**
     * Add a booking to the specificflight
     * @param alID the id of the airline
     * @param fn the flightnumber of the flight
     * @param date the date of the flight
     * @param pID the person id for the booking
     * @param seat the seatnumber on the flight
     * @return true is the booking is creates succesfully
     */
    public boolean addBooking(int alID, int fn, String date, int pID, String seat){
        for(RegularFlight rf : airlines.get(alID).getFlights()){
            for(SpecificFlight sf : rf.getFlights()){
                if(sf.getDate().equals(date)){
                    Person p = null;
                    for(Person pp : persons){
                        if(pp.getId() == pID){
                            p = pp;
                        }
                    }
                    sf.addBooking(new Booking(p, seat));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * returnes a list with the bookings for the flight
     * @param alID the id of the airline
     * @param fn the flightnumber
     * @param date the date of the flight
     * @return the bookings for the flight
     */
    public String bookings(int alID, int fn, String date){
        String result = "";
        for(RegularFlight rf : airlines.get(alID).getFlights()){
            for(SpecificFlight sf : rf.getFlights()){
                if(sf.getDate().equals(date)){
                    for(Booking b : sf.getBookings()){
                        result += sf.toString() + " has " + b.getPassenger() + " on seat " + b.getSeat() + "\n";
                    }
                }
            }
        }
        return result;
    }


}
