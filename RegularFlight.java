import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 9:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class RegularFlight {
    private ArrayList<SpecificFlight> flights = new ArrayList<SpecificFlight>();
    private Airline airline;
    private int flightNumber;
    private String depTime, departure, arrival;

    public RegularFlight(Airline a, int fn, String dt, String d, String arr){
        airline = a;
        flightNumber = fn;
        depTime = dt;
        departure = d;
        arrival = arr;
    }

    public String toString(){
        return "flight " + flightNumber + " from " + airline;
    }

    public void addFlight(SpecificFlight sf){
        flights.add(sf);
    }

    public ArrayList<SpecificFlight> getFlights(){
        return flights;
    }

    public void remFlight(SpecificFlight sf){
        flights.remove(sf);
    }

    public Airline getAirline(){
        return airline;
    }

    public int getFlightNumber(){
        return flightNumber;
    }

    public String getDepTime(){
        return depTime;
    }

    public String getDeparture(){
        return departure;
    }

    public String getArrival(){
        return arrival;
    }
}
