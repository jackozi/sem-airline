import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 9:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class SpecificFlight {
    private ArrayList<Employee> employees= new ArrayList<Employee>();
    private ArrayList<Booking> bookings = new ArrayList<Booking>();
    private String date;
    private RegularFlight rf;

    public SpecificFlight(RegularFlight r, String date) {
        rf = r;
        this.date = date;
    }

    public String toString(){
        return rf + " on " + date;
    }

    public boolean specifyAirplane(){
        return true;
    }

    public void addEmployee(Employee e){
        employees.add(e);
    }

    public void remEmployee(Employee e){
        employees.remove(e);
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    public void addBooking(Booking b){
        bookings.add(b);
    }

    public void remBooking(Booking b){
        bookings.remove(b);
    }

    public ArrayList<Booking> getBookings(){
        return bookings;
    }

    public String getDate(){
         return date;
    }


}
