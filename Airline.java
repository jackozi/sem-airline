import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 9:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class Airline {
    private String name = "";
    private ArrayList<RegularFlight> flights = new ArrayList<RegularFlight>();

    public Airline(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void addFlight(RegularFlight f){
        flights.add(f);
    }

    public void removeFlight(RegularFlight f){
        flights.remove(f);
    }

    public ArrayList<RegularFlight> getFlights(){
        return flights;
    }


}
