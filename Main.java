import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 10:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String[] args){
        Controller c = new Controller();
        System.out.println("Super dual action airline booking system 3000 plus");
        while(true){
            String CurLine = "";;
                  InputStreamReader converter = new InputStreamReader(System.in);
                   BufferedReader in = new BufferedReader(converter);
                         while (!(CurLine.equals("quit"))){
                             System.out.print("> ");
                             try {
                                 CurLine = in.readLine();

                             } catch (IOException e) {
                                 e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                             }

                             if (!(CurLine.equals("quit"))){
                                 String[] cmd = CurLine.split(" ");
                                 if(cmd[0].equals("addPassenger")){
                                     if(cmd.length != 4){
                                         println("Invalid params");
                                     }else{
                                        if(c.addPerson(cmd[1], cmd[2], Integer.valueOf(cmd[3]))){}else
                                             println("Something went wrong");
                                     }

                                 }else if(cmd[0].equals("addAirline")){
                                     if(cmd.length != 2){
                                         println("Invalid params");
                                     }else{
                                         if(c.newAirline(cmd[1])){}else
                                             println("Something went wrong");
                                     }

                                 }else if(cmd[0].equals("addRF")){
                                     if(cmd.length != 6){
                                         println("Invalid params");
                                     }else{
                                         if(c.newRF(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]), cmd[3],  cmd[4], cmd[5])){}else
                                             println("Something went wrong");
                                     }
                                 }else if(cmd[0].equals("addSF")){
                                     if(cmd.length != 4){
                                         println("Invalid params");
                                     }else{
                                         if(c.newSF(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]), cmd[3])){}else
                                             println("Something went wrong");
                                     }

                                 }else if(cmd[0].equals("addBooking")){
                                     if(cmd.length != 6){
                                         println("Invalid params");
                                     }else{
                                         if(c.addBooking(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]), cmd[3],  Integer.valueOf(cmd[4]), cmd[5])){}else
                                             println("Something went wrong");
                                     }

                                 }else if(cmd[0].equals("addEmployee")){

                                 }else if(cmd[0].equals("getBookings")){
                                     if(cmd.length != 4){
                                         println("Invalid params");
                                     }else{
                                         println(c.bookings(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]) ,cmd[3]));
                                     }
                                 }


                       }else System.exit(0);
               }
        }


    }

    private static void println(String print){
        System.out.println(print);
    }
}
