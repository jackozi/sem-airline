/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 12/3/13
 * Time: 3:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class test {

    /**
     * De main class van de test
     * @param args
     */
    public static void main(String[] args){
        //Initiate the controllers for the tests
        Controller c = new Controller();
        addData(c);
        retrieveBookings(c);
        println("Completed all the test cases.");
    }

    private static void addData(Controller c){
        //add initial data to the controller
        assertEquals("Adding airline failed", true, c.newAirline("KLM"));//add KLM as airline with id 0
        assertEquals("Adding second airline failed", true, c.newAirline("EasyJet"));//add EasyJet as airline with id 1
        assertEquals("Adding flight failed", true, c.newRF(0, 1337, "1830", "AMS", "LIS"));//add Regular Flight with Flight number 1337 for KLM
        assertEquals("Adding second flight failed", true, c.newRF(1, 1337, "1830", "HEA", "AMS"));//Add a somewhat similar flight to EasyJet
        assertEquals("Adding specific failed", true, c.newSF(0, 1337, "23/01/2014"));//Adds the specific flight for 23 Januari 2014
        assertEquals("Adding Person failed", true, c.addPerson("Henk", "Twente", 212));//Add a passenger henk from twente with id 212
        assertEquals("Succeeded in adding second person with the same id", false, c.addPerson("Karel", "Amsterdam", 212));//See if we can add a person with the same ID
        assertEquals("Adding booking failed", true, c.addBooking(0, 1337, "23/01/2014", 212, "26A"));//Add a booking for Henk on the KLM flight with a window seat in the back

    }

    private static void retrieveBookings(Controller c){
        //requires addData() to be ran first
        assertEquals("Get the bookings from the system failed", "flight 1337 from KLM on 23/01/2014 has Henk on seat 26A\n", c.bookings(0, 1337, "23/01/2014"));//test if henk seat is booked
    }

    private static boolean assertEquals(String error, Object exp, Object rl){
        if(exp.equals(rl)){
            return true;
        }else
            println(error);
            println("The output was: " + rl);
        return false;

    }

    private static void println(String txt){
        System.out.println(txt);
    }
}
