/**
 * Created with IntelliJ IDEA.
 * User: jacko
 * Date: 11/29/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class Booking {
    private Person passenger;
    private String seat;

    public Booking(Person p, String s){
        passenger = p;
        seat = s;
    }

    public Person getPassenger(){
        return passenger;
    }

    public String getSeat(){
        return seat;
    }
}
